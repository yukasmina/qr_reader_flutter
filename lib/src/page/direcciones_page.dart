import 'package:flutter/material.dart';
import 'package:qrscanner/src/bloc/scan_bloc.dart';
import 'package:qrscanner/src/providers/db_provider.dart';
import 'package:qrscanner/src/utils/utils.dart' as utils;

class DireccionesPage extends StatelessWidget {
  final scanBloc = new ScanBloc();

  @override
  Widget build(BuildContext context) {
    scanBloc.obtenerLista();
    return StreamBuilder<List<ScanModel>>(
        stream: scanBloc.scansStreamHttp,
        builder: (BuildContext context, AsyncSnapshot<List<ScanModel>> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          final scans = snapshot.data;
          if (scans.length == 0) {
            return Center(
              child: Text('No hay informacion'),
            );
          }
          return ListView.builder(
              itemCount: scans.length,
              itemBuilder: (context, i) => Dismissible(
                //permite mover de izquiera derecha
                key: UniqueKey(),
                background: Container(
                  color: Colors.red,
                ),
                onDismissed: (direccion) => scanBloc.borrarScan(scans[i].id),
                child: ListTile(
                  leading: Icon(
                    Icons.cloud_queue,
                    color: Theme.of(context).primaryColor,
                  ),
                  title: Text(scans[i].valor),
                  subtitle: Text('ID: ${scans[i].id}'),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.green,
                  ),
                  onTap: () => utils.launchURL(context, scans[i]),
                ),
              ));
        });
  }
}