import 'package:flutter/material.dart';
import 'package:qrscanner/src/bloc/scan_bloc.dart';
import 'package:qrscanner/src/models/scan_model.dart';
import 'package:qrscanner/src/page/direcciones_page.dart';
import 'package:qrscanner/src/page/maps_page.dart';


import 'package:barcode_scan/barcode_scan.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final scansBloc = new ScanBloc();
  int pagina = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Scanner'),
        actions: <Widget>[IconButton(icon: Icon(Icons.delete_forever), onPressed: scansBloc.borrarScansTodos)],
      ),
      body: _callPage(pagina),
      bottomNavigationBar: _crearBotonesNavigation(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_center_focus),
        onPressed: _scanQR,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _crearBotonesNavigation() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map), title: Text('Maps')),
        BottomNavigationBarItem(icon: Icon(Icons.brightness_5), title: Text('Direcciones'))
      ],
      currentIndex: pagina,
      onTap: (index) {
        setState(() {
          pagina = index;
        });
      },
    );
  }

  Widget _callPage(int paginaActual) {
    switch (paginaActual) {
      case 0:
        return MapsPage();
      case 1:
        return DireccionesPage();
      default:
        return MapsPage();
    }
  }

  _scanQR() async {
    //https://fernando-herrera.com
    //geo:-3.9984362135151317,-79.20612230738529
    dynamic futureString;
    try {
      futureString = await BarcodeScanner.scan();
    } catch (e) {
      futureString = e.toString();
    }

    //print('Future String: ${futureString.rawContent}');
    if (futureString.rawContent != null) {
      //proceso de grabacion de datos
      final scan = ScanModel(valor: futureString.rawContent);
      scansBloc.agregarScans(scan);
    }
  }
}
