import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:qrscanner/src/models/scan_model.dart';

class MapaPage extends StatefulWidget {
  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  MapController mapController = new MapController();
  String tipoMapa = 'streets';

  @override
  Widget build(BuildContext context) {
    final ScanModel scan = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Coordenadas QR'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.my_location),
              onPressed: () {
                mapController.move(scan.getLatLng(), 15);
              })
        ],
      ),
      body: _crearFlutterMap(scan),
      floatingActionButton: _crearBotonFlotante(context),
    );
  }

  Widget _crearFlutterMap(ScanModel scan) {
    return FlutterMap(
      mapController: mapController,
      options: MapOptions(center: scan.getLatLng(), zoom: 15),
      layers: [
        _crearMapa(),
        _crearMarcador(scan),
      ],
    );
  }

  _crearMapa() {
    return TileLayerOptions(
        urlTemplate: 'https://api.mapbox.com/styles/v1/yukasmina/ck6vm31wb0ohu1irinh06pokc/tiles/256/'
            '{z}/{x}/{y}@2x?access_token={accessToken}',
        additionalOptions: {
          'accessToken': 'pk.eyJ1IjoieXVrYXNtaW5hIiwiYSI6ImNrZjVuMWZjdjAwMzIyc202bWx1bXMwM3AifQ.i4YEpY3VMQYR4xt85VHNVw',
          'id': 'mapbox.mapbox-streets-v9'
        });
  }

  _crearMarcador(ScanModel scan) {
    return MarkerLayerOptions(markers: <Marker>[
      Marker(
          width: 100.0,
          height: 100.0,
          point: scan.getLatLng(),
          builder: (context) => Container(
                child: Icon(
                  Icons.location_on,
                  size: 65.0,
                  color: Theme.of(context).primaryColor,
                ),
              ))
    ]);
  }

  Widget _crearBotonFlotante(BuildContext context) {
    return FloatingActionButton(
        onPressed: () {
          if (tipoMapa == 'streets') {
            tipoMapa = 'dark';
          } else if (tipoMapa == 'dark') {
            tipoMapa = 'light';
          } else if (tipoMapa == 'light') {
            tipoMapa = 'outdoors';
          } else if (tipoMapa == 'outdoors') {
            tipoMapa = 'satellite';
          } else {
            tipoMapa = 'streets';
          }
          _snackBar(tipoMapa);
          setState(() {});
        },
        child: Icon(Icons.repeat),
        backgroundColor: Theme.of(context).primaryColor);
  }

  _snackBar(String texto) {
    print("llega");
    return Scaffold.of(context).showSnackBar(SnackBar(content: Text(texto)));
  }
}
