import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'package:qrscanner/src/models/scan_model.dart';
export 'package:qrscanner/src/models/scan_model.dart';

class DBProvider {
  //patron singleton
  static Database _database;
  static final DBProvider db = DBProvider._(); //constructor privado
  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  /*
   * Deteerminar la ubicacion de  la base de datos
   */
  initDB() async {
    Directory documentsDyrectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDyrectory.path, 'ScansDB.db');
    return await openDatabase(path, version: 1, onOpen: (db) {}, onCreate: (Database db, int version) async {
      //creacion de la base d e datos
      await db.execute('CREATE TABLE Scans('
          'id INTEGER PRIMARY KEY,'
          'tipo TEXT,'
          'valor TEXT'
          ')');
    });
  }

  /*
   * Crear registros en la base de datos primera forma
   */
  nuevoScanRow(ScanModel nuevoScan) async {
    final db = await database; //permite determinar si ya esta creado la base de datos
    final res = await db.rawInsert("INSERT Into Scans (id,tipo,valor) "
        "VALUES (${nuevoScan.id},'${nuevoScan.tipo}','${nuevoScan.valor}')");
    return res;
  }

  /*
   * Metodo para ingresar datos segunda forma
   */
  nuevoScan(ScanModel nuevoScan) async {
    final db = await database;
    final res = await db.insert('Scans', nuevoScan.toMap());
    return res;
  }

  //Select_ obtener informacion
  Future<ScanModel> getScanId(int id) async {
    final db = await database;
    final res = await db.query('Scans', where: 'id=?', whereArgs: [id]);
    return res.isNotEmpty ? ScanModel.fromMap(res.first) : null;
  }

  //Select_ obtener informacion lista
  Future<List<ScanModel>> getScanList() async {
    final db = await database;
    final res = await db.query('Scans');
    List<ScanModel> list = res.isNotEmpty ? res.map((c) => ScanModel.fromMap(c)).toList() : [];
    return list;
  }

  //Select_ obtener informacion lista
  Future<List<ScanModel>> getScanTipo(String tipo) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Scans WHERE tipo='$tipo'");
    List<ScanModel> list = res.isNotEmpty ? res.map((c) => ScanModel.fromMap(c)).toList() : [];
    return list;
  }

  /*
   * Metodo para actualizar registros
   *
   */
  Future<int> updateScan(ScanModel updateScan) async {
    final db = await database;
    final res = await db.update('Scans', updateScan.toMap(), where: 'id = ?', whereArgs: [updateScan.id]);
    return res;
  }

  /*
   * Eliminar registros
   */
  Future<int> deleteScan(int id) async {
    final db = await database;
    final res = await db.delete('Scans', where: 'id = ?', whereArgs: [id]);
    return res;
  }

  /*
   * Eliminar todo los registros
   */
  Future<int> deleteScanAll() async {
    final db = await database;
    final res = await db.rawDelete("DELETE FROM Scans");
    return res;
  }
}
