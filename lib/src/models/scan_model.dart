// To parse this JSON data, do
//
//     final scanModel = scanModelFromMap(jsonString);

import 'dart:convert';
import 'package:latlong/latlong.dart';
class ScanModel {
  ScanModel({
    this.id,
    this.tipo,
    this.valor,
  }){
    if(this.valor.contains('http')){
      this.tipo='http';
    }else{
      this.tipo='geo';
    }
  }

  int id;
  String tipo;
  String valor;

  factory ScanModel.fromJson(String str) => ScanModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ScanModel.fromMap(Map<String, dynamic> json) => ScanModel(
    id: json["id"] == null ? null : json["id"],
    tipo: json["tipo"] == null ? null : json["tipo"],
    valor: json["valor"] == null ? null : json["valor"],
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "tipo": tipo == null ? null : tipo,
    "valor": valor == null ? null : valor,
  };
  LatLng getLatLng(){
    final latlng=valor.substring(4).split(',');
    final lat=double.parse(latlng[0]);
    final lng=double.parse(latlng[1]);
    return LatLng(lat,lng);
  }
}