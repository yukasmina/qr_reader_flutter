import 'dart:async';

import 'package:qrscanner/src/bloc/validator.dart';
import 'package:qrscanner/src/models/scan_model.dart';
import 'package:qrscanner/src/providers/db_provider.dart';

class ScanBloc with Validator {
  static final ScanBloc _singleton = new ScanBloc._internal();

  factory ScanBloc() {
    return _singleton; //puede retornar una instancia del singleton
  }

  ScanBloc._internal() {
    ///Obtener scans de la Base de datos
    obtenerLista();
  }

  //creacion de StremCotrller
  final _scansController = StreamController<List<ScanModel>>.broadcast(); //al cear un Stream se debe cerrar las instancias
  Stream<List<ScanModel>> get scansStream => _scansController.stream.transform(validarGeo); //tipo de informacon fluira
  Stream<List<ScanModel>> get scansStreamHttp => _scansController.stream.transform(validarHttp);
  dispose() {
    _scansController?.close();
  }
  /*
   *Permite obtener toda la lsita de http o geo
   */
  obtenerLista() async {
    _scansController.sink.add(await DBProvider.db.getScanList());
  }
  /*
  Metodo que permite agrear los scans
   */
  agregarScans(ScanModel scan) async {
    await DBProvider.db.nuevoScan(scan);
    obtenerLista();
  }

  /*
   *Metodo que permite borrar un escan de acuerdo al id
   */
  borrarScan(int id) async {
    await DBProvider.db.deleteScan(id);
    obtenerLista();
  }

  /*
  Metodo que permite borrar todo loos registros de la db
   */
  borrarScansTodos() async {
    await DBProvider.db.deleteScanAll();
    obtenerLista();
  }
}
